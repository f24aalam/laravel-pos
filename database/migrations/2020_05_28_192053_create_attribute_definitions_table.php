<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributeDefinitionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attribute_definitions', function(Blueprint $table)
		{
			$table->integer('definition_id', true);
			$table->integer('definition_fk')->nullable()->index('definition_fk');
			$table->string('definition_name');
			$table->string('definition_type', 45);
			$table->string('definition_unit', 16)->nullable();
			$table->tinyInteger('definition_flags');
			$table->tinyInteger('deleted')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attribute_definitions');
	}
}
