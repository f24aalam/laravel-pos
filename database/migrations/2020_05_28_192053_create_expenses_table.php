<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('expenses', function(Blueprint $table)
		{
			$table->integer('expense_id', true);
			$table->integer('expense_category_id')->index('expense_category_id');
			$table->integer('employee_id')->index('employee_id');
			$table->integer('supplier_id')->nullable()->index('ospos_expenses_ibfk_3');
			$table->timestamp('date')->nullable()->useCurrent()->index('date');
			$table->decimal('amount', 15);
			$table->string('payment_type', 40);
			$table->string('description');
			$table->integer('deleted')->default(0);
			$table->string('supplier_tax_code')->nullable();
			$table->decimal('tax_amount', 15)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('expenses');
	}
}
