<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGiftcardsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('giftcards', function(Blueprint $table)
		{
			$table->integer('giftcard_id', true);
			$table->integer('person_id')->nullable()->index('person_id');
			$table->timestamp('record_time')->useCurrent();
			$table->string('giftcard_number')->nullable()->unique('giftcard_number');
			$table->decimal('value', 15);
			$table->integer('deleted')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('giftcards');
	}
}
