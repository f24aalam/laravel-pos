<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inventory', function(Blueprint $table)
		{
			$table->integer('trans_id', true);
			$table->integer('trans_items')->default(0)->index('trans_items');
			$table->integer('trans_user')->default(0)->index('trans_user');
			$table->integer('trans_location')->index('trans_location');
			$table->timestamp('trans_date')->useCurrent()->index('trans_date');
			$table->text('trans_comment');
			$table->decimal('trans_inventory', 15, 3)->default(0.000);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inventory');
	}
}
