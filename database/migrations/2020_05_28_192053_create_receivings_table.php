<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReceivingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('receivings', function(Blueprint $table)
		{
			$table->integer('receiving_id', true);
			$table->integer('supplier_id')->nullable()->index('supplier_id');
			$table->integer('employee_id')->default(0)->index('employee_id');
			$table->timestamp('receiving_time')->useCurrent()->index('receiving_time');
			$table->text('comment')->nullable();
			$table->string('payment_type', 20)->nullable();
			$table->string('reference', 32)->nullable()->index('reference');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('receivings');
	}
}
