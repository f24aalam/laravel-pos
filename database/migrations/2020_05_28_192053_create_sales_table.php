<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sales', function(Blueprint $table)
		{
			$table->integer('sale_id', true);
			$table->integer('customer_id')->nullable()->index('customer_id');
			$table->integer('employee_id')->default(0)->index('employee_id');
			$table->integer('dinner_table_id')->nullable()->index('dinner_table_id');
			$table->timestamp('sale_time')->useCurrent()->index('sale_time');
			$table->text('comment')->nullable();
			$table->string('invoice_number', 32)->nullable()->unique('invoice_number');
			$table->string('quote_number', 32)->nullable();
			$table->tinyInteger('sale_status')->default(0);
			$table->string('work_order_number', 32)->nullable();
			$table->tinyInteger('sale_type')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sales');
	}
}
