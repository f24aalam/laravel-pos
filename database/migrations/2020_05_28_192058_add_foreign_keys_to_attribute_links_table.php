<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToAttributeLinksTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('attribute_links', function(Blueprint $table)
		{
			$table->foreign('definition_id', 'attribute_links_ibfk_1')->references('definition_id')->on('attribute_definitions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('attribute_id', 'attribute_links_ibfk_2')->references('attribute_id')->on('attribute_values')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('item_id', 'attribute_links_ibfk_3')->references('item_id')->on('items')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('receiving_id', 'attribute_links_ibfk_4')->references('receiving_id')->on('receivings')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('sale_id', 'attribute_links_ibfk_5')->references('sale_id')->on('sales')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('attribute_links', function(Blueprint $table)
		{
			$table->dropForeign('attribute_links_ibfk_1');
			$table->dropForeign('attribute_links_ibfk_2');
			$table->dropForeign('attribute_links_ibfk_3');
			$table->dropForeign('attribute_links_ibfk_4');
			$table->dropForeign('attribute_links_ibfk_5');
		});
	}
}
