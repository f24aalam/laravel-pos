<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToCustomersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customers', function(Blueprint $table)
		{
			$table->foreign('person_id', 'customers_ibfk_1')->references('person_id')->on('people')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('package_id', 'customers_ibfk_2')->references('package_id')->on('customers_packages')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('sales_tax_code_id', 'customers_ibfk_3')->references('tax_code_id')->on('tax_codes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customers', function(Blueprint $table)
		{
			$table->dropForeign('customers_ibfk_1');
			$table->dropForeign('customers_ibfk_2');
			$table->dropForeign('customers_ibfk_3');
		});
	}
}
