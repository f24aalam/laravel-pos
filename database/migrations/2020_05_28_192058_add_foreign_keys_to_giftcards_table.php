<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGiftcardsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('giftcards', function(Blueprint $table)
		{
			$table->foreign('person_id', 'giftcards_ibfk_1')->references('person_id')->on('people')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('giftcards', function(Blueprint $table)
		{
			$table->dropForeign('giftcards_ibfk_1');
		});
	}
}
