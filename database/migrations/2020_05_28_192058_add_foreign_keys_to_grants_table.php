<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGrantsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('grants', function(Blueprint $table)
		{
			$table->foreign('permission_id', 'grants_ibfk_1')->references('permission_id')->on('permissions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('person_id', 'grants_ibfk_2')->references('person_id')->on('employees')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('grants', function(Blueprint $table)
		{
			$table->dropForeign('grants_ibfk_1');
			$table->dropForeign('grants_ibfk_2');
		});
	}
}
