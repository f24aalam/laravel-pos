<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToInventoryTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('inventory', function(Blueprint $table)
		{
			$table->foreign('trans_items', 'inventory_ibfk_1')->references('item_id')->on('items')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('trans_user', 'inventory_ibfk_2')->references('person_id')->on('employees')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('trans_location', 'inventory_ibfk_3')->references('location_id')->on('stock_locations')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inventory', function(Blueprint $table)
		{
			$table->dropForeign('inventory_ibfk_1');
			$table->dropForeign('inventory_ibfk_2');
			$table->dropForeign('inventory_ibfk_3');
		});
	}
}
