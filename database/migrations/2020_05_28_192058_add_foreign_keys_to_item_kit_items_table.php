<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToItemKitItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('item_kit_items', function(Blueprint $table)
		{
			$table->foreign('item_kit_id', 'item_kit_items_ibfk_1')->references('item_kit_id')->on('item_kits')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('item_id', 'item_kit_items_ibfk_2')->references('item_id')->on('items')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('item_kit_items', function(Blueprint $table)
		{
			$table->dropForeign('item_kit_items_ibfk_1');
			$table->dropForeign('item_kit_items_ibfk_2');
		});
	}
}
