<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToItemsTaxesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('items_taxes', function(Blueprint $table)
		{
			$table->foreign('item_id', 'items_taxes_ibfk_1')->references('item_id')->on('items')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('items_taxes', function(Blueprint $table)
		{
			$table->dropForeign('items_taxes_ibfk_1');
		});
	}
}
