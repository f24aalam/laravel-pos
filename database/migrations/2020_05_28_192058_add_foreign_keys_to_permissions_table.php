<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPermissionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permissions', function(Blueprint $table)
		{
			$table->foreign('module_id', 'permissions_ibfk_1')->references('module_id')->on('modules')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('location_id', 'permissions_ibfk_2')->references('location_id')->on('stock_locations')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permissions', function(Blueprint $table)
		{
			$table->dropForeign('permissions_ibfk_1');
			$table->dropForeign('permissions_ibfk_2');
		});
	}
}
