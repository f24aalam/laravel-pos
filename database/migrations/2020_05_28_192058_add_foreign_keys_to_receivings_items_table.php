<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToReceivingsItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receivings_items', function(Blueprint $table)
		{
			$table->foreign('item_id', 'receivings_items_ibfk_1')->references('item_id')->on('items')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('receiving_id', 'receivings_items_ibfk_2')->references('receiving_id')->on('receivings')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receivings_items', function(Blueprint $table)
		{
			$table->dropForeign('receivings_items_ibfk_1');
			$table->dropForeign('receivings_items_ibfk_2');
		});
	}
}
