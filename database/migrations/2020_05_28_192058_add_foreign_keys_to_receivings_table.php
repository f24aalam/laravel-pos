<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToReceivingsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('receivings', function(Blueprint $table)
		{
			$table->foreign('employee_id', 'receivings_ibfk_1')->references('person_id')->on('employees')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('supplier_id', 'receivings_ibfk_2')->references('person_id')->on('suppliers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('receivings', function(Blueprint $table)
		{
			$table->dropForeign('receivings_ibfk_1');
			$table->dropForeign('receivings_ibfk_2');
		});
	}
}
