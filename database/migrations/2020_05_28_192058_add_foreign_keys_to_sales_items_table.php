<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSalesItemsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sales_items', function(Blueprint $table)
		{
			$table->foreign('item_id', 'sales_items_ibfk_1')->references('item_id')->on('items')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('sale_id', 'sales_items_ibfk_2')->references('sale_id')->on('sales')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('item_location', 'sales_items_ibfk_3')->references('location_id')->on('stock_locations')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sales_items', function(Blueprint $table)
		{
			$table->dropForeign('sales_items_ibfk_1');
			$table->dropForeign('sales_items_ibfk_2');
			$table->dropForeign('sales_items_ibfk_3');
		});
	}
}
