<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSalesPaymentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sales_payments', function(Blueprint $table)
		{
			$table->foreign('sale_id', 'sales_payments_ibfk_1')->references('sale_id')->on('sales')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('employee_id', 'sales_payments_ibfk_2')->references('person_id')->on('employees')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sales_payments', function(Blueprint $table)
		{
			$table->dropForeign('sales_payments_ibfk_1');
			$table->dropForeign('sales_payments_ibfk_2');
		});
	}
}
