<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSalesRewardPointsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sales_reward_points', function(Blueprint $table)
		{
			$table->foreign('sale_id', 'sales_reward_points_ibfk_1')->references('sale_id')->on('sales')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sales_reward_points', function(Blueprint $table)
		{
			$table->dropForeign('sales_reward_points_ibfk_1');
		});
	}
}
