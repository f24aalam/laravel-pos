<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToSalesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sales', function(Blueprint $table)
		{
			$table->foreign('employee_id', 'sales_ibfk_1')->references('person_id')->on('employees')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('customer_id', 'sales_ibfk_2')->references('person_id')->on('customers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('dinner_table_id', 'sales_ibfk_3')->references('dinner_table_id')->on('dinner_tables')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sales', function(Blueprint $table)
		{
			$table->dropForeign('sales_ibfk_1');
			$table->dropForeign('sales_ibfk_2');
			$table->dropForeign('sales_ibfk_3');
		});
	}
}
